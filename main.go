package main

import (
	"encoding/json"
	"fmt"
	"os"
)

// Config is the configuration for the system
type Config struct {
	DeltaT          float64 `json:"delta_t"`
	LowK            float64 `json:"low_k"`
	HighK           float64 `json:"high_k"`
	DeltaK          float64 `json:"delta_k"`
	LowTemp         float64 `json:"low_temp"`
	HighTemp        float64 `json:"high_temp"`
	SurroundingTemp float64 `json:"surrounding_temp"`
	Mass            float64 `json:"mass"`
	C               float64 `json:"c"`
}

// Code starts here
func main() {
	// Open the configuration file that contains the settings and constants for the model
	file, err := os.Open("config.json")
	if err != nil {
		panic(err)
	}

	// Parse the config file into a machine readable format
	cfg := new(Config)
	err = json.NewDecoder(file).Decode(&cfg)
	if err != nil {
		panic(err)
	}

	// Execute run the model for all values of k between LowK and HighK with a step size of DeltaK
	for k := cfg.LowK; k <= cfg.HighK; k = k + cfg.DeltaK {
		// Print the current K we are calculating for
		fmt.Printf("Doing k %v\n", k)

		// Get the approximate time for the given K and config
		t := getTime(cfg.DeltaT, cfg.HighTemp, cfg.LowTemp, cfg.SurroundingTemp, cfg.Mass, cfg.C, k)

		// Get the exact time for the given K and config
		tExact := getTimeExact(cfg.Mass, cfg.C, cfg.HighTemp, cfg.SurroundingTemp, cfg.LowTemp, k)

		// Print the approx and exact time for the given K and config 
		fmt.Printf("Approx : %v\n", t)
		fmt.Printf("Exact: %v\n", tExact)
		fmt.Println("-------")
	}
}

// Luca
func getTime(deltaT, temp, lowTemp, surroundingTemp, mass, C, k float64) float64 {
	// t is the time in seconds
	var t float64

	// qk is the thermal mass of the liquid calculated using the mass, the Cw value and the temperature 
	qk := (mass * C * temp)

	// approximate the value of t by simulating the cooling process
	for {
		// start the iteration by incrementing t with deltaT
		t = t + deltaT

		// calculate the lost energy from the liquid in this step
		stroom1 := k * (temp - surroundingTemp)

		// apply this loss to the total thermal mass
		qk = qk - stroom1*deltaT

		// calculate the current temprature of the liquid
		temp = qk / (mass * C)

		// if the liquid has reached the target temprature return the time this took
		if temp <= lowTemp {
			return t
		}
	}
}

// Jaap
func getTimeExact(mass, c, highT, ambient, targetT, k float64) float64 {
	return (highT - targetT) * (mass * c) / ((highT - ambient) * k)
}
